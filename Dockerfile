FROM node:8

MAINTAINER Budgetbakers "info@budgetbakers.com"
USER root

# usage:
#    Default host OS uid:guid=1001:1001
#    sudo docker build -t docker-web-base .
#    Custom host OS uid:guid
#    sudo docker build -t docker-web-base --build-arg host_uid=1666 --build-arg host_guid=1666 .

ARG host_uid
ARG host_guid

# Required for Git+SSH otherwise it would raise errors regarding git access
# which is not obviously related to unaccessible SSH keys
# uid & guid must match Docker uid & guid, user/group name 'jenkins' is just mandatory
# argument of groupadd/useradd and might be arbitrary valid value.
RUN groupadd --gid ${host_guid:-1001} jenkins \
# --create-home is required because of: warning Cannot find a suitable global folder. Tried these: "/usr/local, /home/jenkins/.yarn"
  && useradd --uid ${host_uid:-1001} --gid ${host_guid:-1001} --create-home jenkins

#
# Installing tools
#


# Note: yarn and git are part of node Docker image. Warn: alpine or other slim images must not be used

# Install Chromium for Karma tests,  chromium-l10n is optional, todo check if it could be removed
RUN apt-get update && apt-get install -y chromium chromium-l10n && apt-get clean && rm -rf /var/lib/apt/lists /var/cache/apt
RUN ln -s $(which chromium)  $(which chromium)-browser

# ERROR: The container started but didn't run the expected command. Please double check your ENTRYPOINT .....
# See https://issues.jenkins-ci.org/browse/JENKINS-49278 it's Jenkins bug that can't be fixed with ENTRYPOINT or CMD
CMD [ "bash", "-c", "echo Node version: $(node -v), yarn version: $(yarn -v)" ]
