# Jenkins Pipeline Docker agent 

See [Customizing the execution environment](https://jenkins.io/doc/book/pipeline/docker/#execution-environment)
just replace `node:7-alpine` with `docker-web-base` used as a tag below:

## Build

```bash
sudo docker build -t docker-web-base .
```

## Test

```bash
sudo docker run --rm docker-web-base
```

